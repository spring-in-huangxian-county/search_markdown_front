# 检索gitmarkdown

## 介绍

检索gitmarkdown

## 运行前
将request.js文件里的后端ip改掉
将vue.config.js的后端ip改掉


## 项目部署

- 执行命令,安装依赖

```
npm install
```

- 启动项目命令

```
npm run serve
```

- 构建项目命令

```
npm run build
```

- 修复代码 lint 错误命令

```
npm run lint
```


