module.exports = {
    // 打包时启用
    // publicPath: './',
    lintOnSave: false,
    devServer: {
      disableHostCheck: true,
      open: true,
      port: 9019,
      proxy: {
        '^/': {
          ws: false,
          target: 'http://10.160.1.77:9000/back',
          changeOrigin: true,
          secure: false,
          pathRewrite: {
            '^/': '/'
          }
        }
      }
    },
  };

